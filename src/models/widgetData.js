export const widgetOptions=[
    {
        key:"oee",
        title:"Overal Equipment Effectiveness",
        description:"Overall Equipment Effectiveness (OEE) measures a machine’s or production platform’s level of productivity. It combines the three key elements together"+
        "Availability - How often is the machine available during its planned time."+
        "Performance - How many units can the machine produce."+
        "Quality - How many defect-free units do the machine produce.",
        chart:['bar','line'],
        img:"../../../public/charts/oee.png" 
    },
    {
        key:"machineUprate",
        title:"Machine Uptime Rate",
        description:" Inadequate performance can indicate poor maintenance, slow change overtimes, or machinery defects that must be addressed.",
        chart:['bar','line'],
        img:"../../../public/charts/machineTimes.png"
    },
    {
        key:"capacityUtilization",
        title:"Capacity Utilization",
        description:"Assessing the rate at which potential output rates are met or not is known as Capacity Utilization. This lets manufacturing companies align the number of units produced with a percentage of potential that can be measured.",
        chart:['bar','line'],
        img:"../../../public/charts/line.png"
    },
    {
        key:"overtimeHours",
        title:"Overtime hours",
        description:"Tracking and analyzing the amount of overtime versus the regular / planned hours can provide insights into scheduling and production issues.",
        chart:['bar','line'],
        img:"../../../public/charts/bars.png"
    },
    {
        key:"downtime",
        title:"Downtime Machines",
        description:"Machine Stopsssss",
        chart:['bar','line','pie'],
        img:"../../../public/charts/bars.png"
    }
]

export const chartTypes = [
    {
        type: "bar",
        name: "Bar",
        icon: "mdi-chart-bar",
        img: "../../../public/charts/barChart.png",
      },
      {
        type: "line",
        name: "Line",
        icon: "mdi-chart-bell-curve-cumulative",
        img: "../../../public/charts/lineChart.png",
      },
      {
        type: "pie",
        name: "Pie",
        icon: "mdi-chart-pie-outline",
        img: "../../../public/charts/pieChart.png",
      },
]

export const periodList = [
    {
        title:"Today",
        value:"Today",

    },
    {
        title:"This week",
        value:"thisWeek", 
    },
    {
        title:"Last week",
        value:"lastWeek", 
    },
    {
        title:"This month",
        value:"thisMonth", 
    },
    {
        title:"Last month",
        value:"lastMonth", 
    } ,
    {
        title:"Last year",
        value:"lastYear", 
    },
    {
        title:"This year",
        value:"thisYear", 
    }
]