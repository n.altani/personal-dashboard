
export const rowData=[
{
  widget:'oee',
  labels: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
  datasets: [
    {
      label: 'oee',
      backgroundColor: '#f87979',
      data: [40, 20, 12, 39, 10, 40, 39, 80, 40, 20, 12, 11],
    },
    {
      label: 'target',
      backgroundColor: '#41B883',
      data: [90,90, 90, 90, 90, 90, 90,90, 90, 90, 90, 90],
    },
  ],
},
{
  widget:'machineUprate',
  labels: [
    '01/07/2023',
    '02/07/2023',
    '03/07/2023',
    '04/07/2023',
    '05/07/2023',
    '06/07/2023',
    '07/07/2023',
    '08/07/2023',
    '09/07/2023',
    '10/07/2023'
  ],
  datasets: [
    {
      backgroundColor: ['#41B883'],
      data: [70, 80, 82, 79, 50, 81, 90, 80, 40, 20, 92],
    },
  ],
},
{
  widget:'capacityUtilization',
  labels: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
  datasets: [
    {
      backgroundColor: ['#41B883'],
      data: [70, 80, 82, 79, 50, 81, 90, 80, 40, 20, 92,96],
    },
  ],
},
{
  widget:'overtimeHours',
  labels: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
  datasets: [
    {
      backgroundColor: ['#41B883'],
      data: [12, 24, 1, 6, 5, 7,12, 10, 10,10, 2,6],
    },
  ],
},
{
  widget:'downtime',
  labels: [
    'Without production need',
    'Returning bar change',
    '3D Layer cat',
    'Mechanical failure',
    'Cleaning',
    'Product changeover',
    '1ST Layer cat', 
  ],
  datasets: [
    {
      
      backgroundColor: ['#e0e1dd',"#2a9d8f",'#e9c46a','#8ecae6','#264653','#a2d2ff','#fb8500'],
      data: [24, 5, 2, 6, 7,12, 10],
    },
  ],
},
]