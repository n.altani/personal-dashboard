// Utilities
import { defineStore } from 'pinia'

export const useAppStore = defineStore('app', {
  state: () => ({
    dashboards:[
    {
      id:1,
      title:'Production Gr'
    },
    {
      id:2,
      title:'Production BG'
    }],
    dashboard:[]
  }),
  actions:{
    onSetDashboard(data){
      this.dashboard = [...data]
    },
    onAddChart(data){
      this.dashboard.push(data)
    },
    onAddDashboard(data){
      this.dashboards.push(data)
    },
    onSetDashboards(data){
      this.dashboards= [...data]
    }
  }
})
